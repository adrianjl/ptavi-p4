#!/usr/bin/python3
"""Clase (y programa principal) para un servidor de eco en UDP simple ."""

import socketserver
import sys
import json
from time import time, strftime, gmtime


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """Echo server class ."""

    diccionario = {}

    def register2json(self):
        """Ejercicio 9 ."""
        filejson = "registered.json"
        with open(filejson, 'w') as jsonfile:
            json.dump(self.diccionario, jsonfile, indent=4)

    def json2register(self):
        """Ejercicio 10 ."""
        try:
            with open('registered.json', 'r') as jsonfile:
                self.dicc = json.load(jsonfile)
        except FileNotFoundError:
            pass

    def handle(self):
        """handle method of the server class (all requests will be handled by this method) ."""
        self.wfile.write(b"Hemos recibido tu peticion")
        line = self.rfile.read()
        metodo = line.decode('utf-8')
        SERVER = self.client_address[0]
        PORT = self.client_address[1]
        DIR = 'SERVER: ' + str(SERVER) + '  ' + 'PORT:' + str(PORT)
        REG = metodo.split(" ")[0]
        CORREO = metodo.split(" ")[2]
        EXP = metodo.split(" ")[3].split("\r\n")[1].split(":")[1]
        # TIEMPO = "EXPIRA EN: " + EXPIRACION
        ZONA = '%Y-%m-%d %H:%M:%S'
        SUMA = int(EXP) + int(time())
        HORA = strftime(ZONA, gmtime(SUMA))

        for line in self.rfile:
            print("El cliente nos manda ", metodo)

        if REG == 'register':
            self.diccionario[CORREO] = DIR, HORA

        if int(EXP) == 0:
            print("Se eliminará" + " " + CORREO)
            del self.diccionario[CORREO]

        print(DIR)
        self.register2json()
        self.json2register()
        print(self.diccionario)


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request

    PORT = int(sys.argv[1])

    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
