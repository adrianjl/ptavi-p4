#!/usr/bin/python3
"""Programa cliente UDP que abre un socket a un servidor."""

import socket
import sys


# SERVER = 'localhost'
# PORT = 6001
# LINE = '¡Hola mundo!'
try:
    SERVER = sys.argv[1]
    PORT = int(sys.argv[2])
    # LINE = str(sys.argv[3:])
    REG = str(sys.argv[3])
    CORREO = str(sys.argv[4])
    EXP = int(sys.argv[5])
    DIR = REG + " SIP: " + CORREO + " SIP/2.0" "\r\n" + "Expires:" + str(EXP) + "\r\n"

except IndexError:
    sys.exit("Debes usar: python3 client.py ip puerto register correo exp")


# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    print("Enviando:", DIR)
    my_socket.send(bytes(DIR, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
